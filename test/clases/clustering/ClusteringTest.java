package clases.clustering;

import clases.Clustering;
import clases.grafos.GrafoVacio;
import org.junit.Before;
import org.junit.Test;

public class ClusteringTest {

  private Clustering clustering;
  private GrafoVacio grafoVacio;

  @Before
  public void setUp() {
    grafoVacio = new GrafoVacio();
  }

  //Exceptions
  @Test(expected = RuntimeException.class)
  public void distanciaCero() {
    clustering = new Clustering(0);
  }

  @Test(expected = RuntimeException.class)
  public void distanciaNegativa() {
    clustering = new Clustering(-1);
  }

}
