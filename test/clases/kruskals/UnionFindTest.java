package clases.kruskals;

import java.util.Set;
import metodosAuxiliares.VerticesDePrueba;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class UnionFindTest {

  private UnionFind unionFind;
  private Set<Coordinate> vertices;
  private int cantVertices;

  @Before
  public void SetUp() {
    vertices = VerticesDePrueba.obtenerVertices();
    unionFind = new UnionFind(vertices);
    cantVertices = vertices.size();
  }

  @Test
  public void union(){
    Coordinate v1 = VerticesDePrueba.obtenerIesimoVertice(0);
    Coordinate v2 = VerticesDePrueba.obtenerIesimoVertice(1);
    Coordinate v3 = VerticesDePrueba.obtenerIesimoVertice(2);
    Coordinate v4 = VerticesDePrueba.obtenerIesimoVertice(3);
    Coordinate v5 = VerticesDePrueba.obtenerIesimoVertice(4);
    
    //v1 <--- v2 <--- v3
    unionFind.union(v1, v2);
    unionFind.union(v2, v3);
    //v4 <--- v5
    unionFind.union(v4, v5);
    
    //v1 <--- v2 <--- v3
    //v1 <--- v4 <--- v5
    unionFind.union(v3, v5);
    
    Assert.assertTrue(unionFind.sameComponent(v2, v4));
    
  }

  //Happy Tests
  @Test
  public void sameComponent() {
    Coordinate primerVertice = VerticesDePrueba.obtenerIesimoVertice(0);
    Coordinate ultimoVertice = VerticesDePrueba.obtenerIesimoVertice(cantVertices - 1);

    //Diferente Componente
    Assert.assertFalse(unionFind.sameComponent(primerVertice, ultimoVertice));
    //Igual Componente
    Assert.assertTrue(unionFind.sameComponent(primerVertice, primerVertice));

  }

  @Test
  public void findAlCrearSets() {
    for (Coordinate vertice : vertices) {
      Coordinate padre = unionFind.find(vertice);
      
      Assert.assertTrue(vertice.equals(padre));
    }
  }

  @Test
  public void crearDisjointSets() {
    //Son sus propios padres
    for (Coordinate vertice : unionFind.getDisjointSets().keySet()) {
      Coordinate padre = unionFind.getDisjointSets().get(vertice);
      
      Assert.assertTrue(vertice.equals(padre));
    }
  }

}
