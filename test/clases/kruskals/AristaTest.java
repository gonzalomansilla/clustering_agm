package clases.kruskals;

import clases.grafos.Arista;
import metodosAuxiliares.VerticesDePrueba;
import org.junit.Assert;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class AristaTest {

  public AristaTest() {
  }

  @Test
  public void Equals() {
    Coordinate v1 = VerticesDePrueba.obtenerIesimoVertice(0);
    Coordinate v2 = VerticesDePrueba.obtenerIesimoVertice(0);
    
    Coordinate v3 = VerticesDePrueba.obtenerIesimoVertice(1);
    
    Coordinate v4 = VerticesDePrueba.obtenerIesimoVertice(2);
    
    Arista a1 = new Arista(v1, v2, 0);
    Arista a2 = new Arista(v1, v2, 0);
    Arista a3 = new Arista(v1, v3, 10);
    Arista a4 = new Arista(v1, v4, 10);
    
    Assert.assertTrue(a1.equals(a2));
    Assert.assertFalse(a1.equals(a3));
    Assert.assertFalse(a1.equals(a3));
    Assert.assertFalse(a1.equals(a4));
    Assert.assertFalse(a3.equals(a4));

  }

}
