package clases.kruskals;

import clases.grafos.AGM;
import clases.grafos.GrafoCompleto;
import java.util.Set;
import metodosAuxiliares.VerticesDePrueba;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class KruskalsTest {
  
  private GrafoCompleto grafoCompleto;
  private Kruskals kruskals;
  
  @Before
  public void SetUp(){
    Set<Coordinate> vertices = VerticesDePrueba.obtenerVertices();
    grafoCompleto = new GrafoCompleto();
    grafoCompleto.crearGrafoCompletoDe(vertices);
    kruskals = new Kruskals();
  }

  @Test
  public void kruskalsAlgorithm() {
    AGM agm = kruskals.kruskalsAlgorithm(grafoCompleto);
    
    int aristasDeAGM = agm.getAristas().size();
    int cantVertices = grafoCompleto.obtenerVertices().size();
    
    Assert.assertEquals(aristasDeAGM, cantVertices - 1);
  }
  
}
