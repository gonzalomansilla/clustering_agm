package clases.grafos.grafoCompleto;

import clases.grafos.GrafoCompleto;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import metodosAuxiliares.VerticesDePrueba;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class GrafoCompletoTest {

  Set<Coordinate> vertices;
  private GrafoCompleto grafoCompleto;

  @Before
  public void setUp() {
    grafoCompleto = new GrafoCompleto();
    vertices = VerticesDePrueba.obtenerVertices();
    grafoCompleto.crearGrafoCompletoDe(vertices);
  }

  @Test
  public void generarAristas() {
    grafoCompleto.crearGrafoCompletoDe(vertices);
    int cantVertices = vertices.size();

    //67 / 2 = 33.5
    double calcAux = (cantVertices - 1) / 2 + 0.5;
    System.out.println(calcAux);

    int aristasEsperadas = (int) (cantVertices * calcAux);
    System.out.println(aristasEsperadas);
    int aristasGeneradas = grafoCompleto.getAristas().size();

    Assert.assertEquals(aristasEsperadas, aristasGeneradas);
  }

  //Happy paths
  @Test
  public void cantidadCorrectaDeVecinosAlCrearGrafo() {
    grafoCompleto.crearGrafoCompletoDe(vertices);

    int cantVecinosEsparada = vertices.size() - 1;
    int cantVecinosCreados = cantVecinosPrimerVertice();

    Assert.assertEquals(cantVecinosEsparada, cantVecinosCreados);
  }

  @Test
  public void establecerValoresDeDistancia() {
    grafoCompleto.crearGrafoCompletoDe(vertices);

    for (HashMap<Coordinate, Double> vecinosDeVertices : grafoCompleto.obtenerGrafo().values()) {

      ArrayList<Double> distancias = new ArrayList<>(vecinosDeVertices.values());
      for (Double distancia : distancias) {
        Assert.assertNotEquals(null, distancia);
      }
    }

  }

  @Test
  public void crearGrafoCompleto() {
    grafoCompleto.crearGrafoCompletoDe(vertices);

    Assert.assertNotEquals(0, grafoCompleto.obtenerGrafo().size());
  }

  //Funciones auxiliares
  private int cantVecinosPrimerVertice() {
    return grafoCompleto.obtenerGrafo().get(VerticesDePrueba.obtenerIesimoVertice(0)).size();
  }

}
