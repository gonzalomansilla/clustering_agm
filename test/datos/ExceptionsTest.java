package datos;

import java.io.File;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

public class ExceptionsTest {

  private LectorDeCoordenadas lector;

  @Before
  public void setUp() {
  }

  @Test(expected = IOException.class)
  public void excepcionArchivoDesconocido() throws IOException {
    //El fichero no existe
    lector = new LectorDeCoordenadas(new File("src/instancias/instancia.txt"));
    lector.obtenerCoordenadas();
  }

}
