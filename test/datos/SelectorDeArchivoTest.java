package datos;

import java.io.File;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

public class SelectorDeArchivoTest {

  private SelectorDeArchivo selector;

  @Before
  public void SetUp() {
    selector = new SelectorDeArchivo();
  }

  @Test
  public void abrirArchivo() {
    // Se abrira el explorador de archivos
    File f = selector.seleccionarArchivo();

    Assert.assertNotNull(f);
  }

}
