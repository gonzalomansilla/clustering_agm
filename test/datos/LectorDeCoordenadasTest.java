package datos;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class LectorDeCoordenadasTest {

  private LectorDeCoordenadas lector;

  @Before
  public void setUp() {
    lector = new LectorDeCoordenadas(new File("src/instancias/instancia1.txt"));
  }

  @Test
  public void obtenerCoords() {
    ArrayList<Coordinate> coords = new ArrayList<>();

    try {
      coords.addAll(lector.obtenerCoordenadas());
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }

    assertNotEquals(0, coords.size());
  }
}
