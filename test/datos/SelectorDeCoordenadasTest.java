package datos;

import clases.grafos.GrafoVacio;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

public class SelectorDeCoordenadasTest {
  
  public SelectorDeCoordenadas selector;
  
  @Before
  public void SetUp(){
   selector = new SelectorDeCoordenadas();
  }

  @Test
  public void seleccionarArchivo() throws Exception {
    //Se abrira el explorador de archivos
    GrafoVacio grafo = selector.obtenerCoordendasDeArchivo();
    
    Assert.assertFalse(grafo.getVertices().isEmpty());
  }
  
}
