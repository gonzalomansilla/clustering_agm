package metodosAuxiliares;

import datos.LectorDeCoordenadas;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class VerticesDePrueba {

  private static Set<Coordinate> vertices = leerVertices();
  
  private static Set<Coordinate> leerVertices() {
    LectorDeCoordenadas lector = new LectorDeCoordenadas(new File("src/instancias/instancia1.txt"));

    Set<Coordinate> resul = null;
    try {
      resul = lector.obtenerCoordenadas();
    }
    catch (IOException ex) {
      System.out.println(ex.getMessage());
    }

    return resul;
  }
  
  public static Set<Coordinate> obtenerVertices(){
    return new HashSet(vertices);
  }

  public static Coordinate obtenerIesimoVertice(int _i) {
    return vertices.toArray(new Coordinate[vertices.size()])[_i];
  }

}
