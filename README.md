# Clustering con AGM (Árbol Generador Mínimo)

_Trabajo Practico n°2 para la materia Programación III_

_En este trabajo se utilizó Grafos para representar en el mapa las ubicaciones y las aristas de diversas instancias de puntos de coordenadas distribuidos en el mismo, donde se 
podrá calcular y visualizar los clusteres generados en función de un parámetro, 
que es una distancia euclidiana base que servirá para calcular los distintos conjuntos de coordenadas._

**Integrantes:** Gonzalo Mansilla

## IREP

- No será posible seleccionar archivos que no sean **.txt** o **.json**
- No será posible generar un **AGM** si no se selecciono un archivo de coordenadas
- El parámetro de **distancia significativa** debe ser mayor a cero.

## Decisiones tomadas

- El parámetro de `distancia` debe ser mayor a 0
- Si no se ingresa una `distancia` no se realizara nada hasta que se ingrese una.
- El JMapViewer se lo instancio en una clase `Mapa` junto con los métodos para insertar/eliminar vértices y aristas
- Para usar las Instancias de coordenadas en un principio se tenia un `ComboBox` con todas ellas, pero luego esto daría problemas en cuanto a la carga de una nueva instancia, así que se opto por agregar un botón para elegir un archivo cargado de coordenadas para luego procesarlo y obtener todas ellas.
- En cuanto al criterio para eliminar las aristas mayores predominantes con respecto a sus vecinos, primero se decidió que tan grande debería ser una Arista con respecto a cada uno de sus pares, donde esta fue si la **diferencia** entre ambas superaba una `distancia` ingresada por el usuario se aumentaba la probabilidad de tomarla como una Arista predominante entre ellos. Si dicha probabilidad era **mayor o igual al 50%** se la consideraba una arista predominante.
- Para **disminuir el tiempo** que tarda en generar todo el proceso de clustering se guardo el último  Grafo ingresado y sus respectivos **grafo completo** y **AGM**, ya que si se prefiere trabajar sobre uno y solo modificar los parámetros no es necesario volver a generar y procesar el grafo.

## Implementación

- Para generar un Árbol Generador Mínimo se opto por el ***[Algoritmo de Kruskals](https://es.wikipedia.org/wiki/Algoritmo_de_Kruskal****)***.
- Para seleccionar un archivo del equipo se prefirió `JFileChooser` de la librería de Java.
- Para el mapa se utilizó ***[JMapViewer](https://wiki.openstreetmap.org/wiki/JMapViewer)***.

![Imagen 1][1] ![Imagen 2][2] ![Imagen 3][3] ![Imagen 4][4]

  [1]: https://i.imgur.com/J56h1wZ.png
  [2]: https://i.imgur.com/YKsUHxa.png
  [3]: https://i.imgur.com/cUetlfJ.png
  [4]: https://i.imgur.com/VetccdJ.png
