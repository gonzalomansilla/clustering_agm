package datos;

import clases.grafos.GrafoVacio;
import java.io.IOException;

public class SelectorDeCoordenadas {

  private LectorDeCoordenadas lector;
  private SelectorDeArchivo selector;

  //Agregar Tipos de de archivos y las excepciones
  
  public SelectorDeCoordenadas() {
    lector = new LectorDeCoordenadas();
    selector = new SelectorDeArchivo();
  }

  public GrafoVacio obtenerCoordendasDeArchivo() throws IOException {
    lector.setArchivoCoordendas(selector.seleccionarArchivo());

    return new GrafoVacio(lector.obtenerCoordenadas());

  }

}
