package datos;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class SelectorDeArchivo {

  private JFileChooser fileChooser;
  private File archivoSeleccionado;

  public SelectorDeArchivo() {
    fileChooser = new JFileChooser();
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setFileFilter(new FileNameExtensionFilter("Coordinates files", "txt", "json"));
    fileChooser.setDialogTitle("Seleccionar archivo de coordenadas a importar");
  }

  public File seleccionarArchivo() {
    int valueFile = fileChooser.showOpenDialog(null);

    if (valueFile == JFileChooser.APPROVE_OPTION)
      archivoSeleccionado = fileChooser.getSelectedFile();
    else
      throw new RuntimeException("Error de archivo");

    return archivoSeleccionado;
  }

  public File getArchivoSeleccionado() {
    return archivoSeleccionado;
  }

}
