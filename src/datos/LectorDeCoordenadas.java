package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class LectorDeCoordenadas {

  private File archivoCoordendas;

  public LectorDeCoordenadas() {
  }

  public LectorDeCoordenadas(File _archivo) {
    archivoCoordendas = _archivo;
  }

  public Set<Coordinate> obtenerCoordenadas() throws IOException {
    Set<Coordinate> coordenadas = new HashSet<>();
    
    BufferedReader buffer = new BufferedReader(new FileReader(archivoCoordendas));

    String line;
    while ((line = buffer.readLine()) != null) {
      String[] coordenada = obtenerCoordenadaDeLineaDeTexto(line);

      if (coordenada.length == 2)
        coordenadas.add(new Coordinate(
                Double.parseDouble(coordenada[0]),
                Double.parseDouble(coordenada[1])
        ));
    }
    return coordenadas;
  }

  public File getArchivoCoordendas() {
    return archivoCoordendas;
  }

  public void setArchivoCoordendas(File _archivo) {
    this.archivoCoordendas = _archivo;
  }

  private String[] obtenerCoordenadaDeLineaDeTexto(String _line) {
    //Quita los posibles espacios al inicio del String
    _line = _line.replaceAll("^\\s+", "");
    return _line.split("\\s+");
  }

}
