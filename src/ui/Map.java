package ui;

import clases.Clustering;
import clases.Mapa;
import clases.grafos.GrafoVacio;
import datos.SelectorDeCoordenadas;
import java.io.IOException;
import javax.swing.JOptionPane;

public class Map extends javax.swing.JFrame {

  private SelectorDeCoordenadas selector = new SelectorDeCoordenadas();
  private Mapa mapa;
  private GrafoVacio grafoSeleccionado;
  private Clustering clustering;

  public Map() {
    initComponents();

    iniciarMapa();

    clustering = new Clustering();
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panelMap = new javax.swing.JPanel();
    panelControl = new javax.swing.JPanel();
    btnGenerar = new javax.swing.JButton();
    txtParametroDistancia = new javax.swing.JTextField();
    jLabel1 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    btnSeleccionarArchivo = new javax.swing.JToggleButton();
    jScrollPane1 = new javax.swing.JScrollPane();
    txtResultado = new javax.swing.JTextArea();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setName("mainFrame"); // NOI18N
    setResizable(false);

    panelMap.setBackground(new java.awt.Color(97, 101, 104));

    javax.swing.GroupLayout panelMapLayout = new javax.swing.GroupLayout(panelMap);
    panelMap.setLayout(panelMapLayout);
    panelMapLayout.setHorizontalGroup(
      panelMapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 993, Short.MAX_VALUE)
    );
    panelMapLayout.setVerticalGroup(
      panelMapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 716, Short.MAX_VALUE)
    );

    panelControl.setBackground(new java.awt.Color(238, 238, 238));

    btnGenerar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    btnGenerar.setText("Generar Clusters");
    btnGenerar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnGenerarActionPerformed(evt);
      }
    });

    txtParametroDistancia.setText("0.001");
    txtParametroDistancia.setToolTipText("Debe ser mayor a 0");

    jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(51, 51, 51));
    jLabel1.setText("Parametros:");

    jLabel3.setForeground(new java.awt.Color(51, 51, 51));
    jLabel3.setText("Peso significativo base:");

    btnSeleccionarArchivo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    btnSeleccionarArchivo.setText("Seleccionar archivo");
    btnSeleccionarArchivo.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnSeleccionarArchivoActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout panelControlLayout = new javax.swing.GroupLayout(panelControl);
    panelControl.setLayout(panelControlLayout);
    panelControlLayout.setHorizontalGroup(
      panelControlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(panelControlLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(panelControlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(btnSeleccionarArchivo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(txtParametroDistancia, javax.swing.GroupLayout.Alignment.TRAILING)
          .addComponent(btnGenerar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addGroup(panelControlLayout.createSequentialGroup()
            .addGroup(panelControlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel1)
              .addComponent(jLabel3))
            .addGap(0, 0, Short.MAX_VALUE)))
        .addContainerGap())
    );
    panelControlLayout.setVerticalGroup(
      panelControlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(panelControlLayout.createSequentialGroup()
        .addGap(40, 40, 40)
        .addComponent(btnSeleccionarArchivo, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(59, 59, 59)
        .addComponent(jLabel1)
        .addGap(18, 18, 18)
        .addComponent(jLabel3)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(txtParametroDistancia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(18, 18, 18)
        .addComponent(btnGenerar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    txtResultado.setColumns(20);
    txtResultado.setLineWrap(true);
    txtResultado.setRows(5);
    txtResultado.setTabSize(4);
    txtResultado.setToolTipText("Resultado...");
    txtResultado.setWrapStyleWord(true);
    txtResultado.setAutoscrolls(false);
    txtResultado.setMargin(new java.awt.Insets(8, 8, 8, 8));
    jScrollPane1.setViewportView(txtResultado);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(panelMap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(18, 18, 18)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
          .addComponent(panelControl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addContainerGap(19, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addComponent(panelControl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addComponent(panelMap, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addContainerGap())
    );

    pack();
    setLocationRelativeTo(null);
  }// </editor-fold>//GEN-END:initComponents

  private void btnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarActionPerformed
    if (seSeleccionoArchivo()) {
      if (parametrosSonCorrectos()) {
        double distancia = Double.parseDouble(txtParametroDistancia.getText());

        boolean huboException = false;
        try {
          clustering.setParametros(distancia);
        }
        catch (RuntimeException e) {
          warningMsj(e.getMessage());
          huboException = true;
        }

        if (!huboException)
          mapa.insertarGrafo(clustering.iniciarClustering(grafoSeleccionado));
      }
    }
    mostrarInformacionClustering();

  }//GEN-LAST:event_btnGenerarActionPerformed

  private void btnSeleccionarArchivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarArchivoActionPerformed
    try {
      grafoSeleccionado = selector.obtenerCoordendasDeArchivo();
      mapa.insertarVertices(grafoSeleccionado.getVertices());
    }
    catch (IOException e) {
      warningMsj(e.getMessage());
    }

  }//GEN-LAST:event_btnSeleccionarArchivoActionPerformed

  public static void main(String args[]) {
    /*
     * Set the Nimbus look and feel
     */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /*
     * If Nimbus (introduced in Java SE 6) is not available, stay with the
     * default look and feel.
     * For details see
     * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    }
    catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(Map.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(Map.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(Map.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(Map.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /*
     * Create and display the form
     */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new Map().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnGenerar;
  private javax.swing.JToggleButton btnSeleccionarArchivo;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JPanel panelControl;
  private javax.swing.JPanel panelMap;
  private javax.swing.JTextField txtParametroDistancia;
  private javax.swing.JTextArea txtResultado;
  // End of variables declaration//GEN-END:variables

  private void iniciarMapa() {
    mapa = new Mapa(panelMap.getSize().width, panelMap.getSize().height);
    panelMap.add(mapa.getMapa());
  }

  private boolean parametrosSonCorrectos() {
    try {
      Double.parseDouble(txtParametroDistancia.getText());
    }
    catch (NumberFormatException e) {
      warningMsj("Error de parametros: " + e.getMessage());

      return false;
    }
    return true;
  }

  private void warningMsj(String _msj) {
    JOptionPane.showMessageDialog(
            this,
            _msj,
            "Operación invalida",
            JOptionPane.WARNING_MESSAGE
    );
  }

  private boolean seSeleccionoArchivo() {
    if (grafoSeleccionado == null) {
      warningMsj("No se seleccionó un archivo");
      return false;
    }

    return true;
  }

  private void mostrarInformacionClustering() {
    StringBuffer res = new StringBuffer();

    res.append("Aristas eliminadas: ")
            .append(String.valueOf(clustering.getAristasEliminadas()))
            .append("\n");

    txtResultado.setText(res.toString());
  }

}
