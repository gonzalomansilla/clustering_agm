package clases.grafos;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Arista implements Comparable<Arista> {

  private Coordinate v1;
  private Coordinate v2;
  private double peso;

  public Arista(Coordinate _v1, Coordinate _v2, double _peso) {
    v1 = _v1;
    v2 = _v2;
    peso = _peso;
  }

  public Coordinate getV1() {
    return v1;
  }

  public void setV1(Coordinate v1) {
    this.v1 = v1;
  }

  public Coordinate getV2() {
    return v2;
  }

  public void setV2(Coordinate v2) {
    this.v2 = v2;
  }

  public double getPeso() {
    return peso;
  }

  public void setPeso(double peso) {
    this.peso = peso;
  }

  @Override
  public String toString() {
    return "Arista: " + "v1=" + v1 + "-  v2=" + v2 + " - peso=" + peso;
  }

  @Override
  public boolean equals(Object _o) {
    if (_o != null) {
      Arista a2 = (Arista) _o;
      if (v1.equals(a2.getV2()) || v1.equals(a2.getV1())) {
        if (v2.equals(a2.getV1()) || v2.equals(a2.getV2())) {
          return Double.compare(peso, a2.getPeso()) == 0;
        }
        return false;
      }
      return false;
    }
    return false;
  }

  @Override
  public int compareTo(Arista _arista) {
    if (peso > _arista.getPeso())
      return 1;
    else if (_arista.getPeso() > peso)
      return -1;
    else
      return 0;
  }

}
