package clases.grafos;

import java.util.HashSet;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class AGM {

  private Set<Arista> aristas;

  public AGM() {
    aristas = new HashSet<>();
  }

  public void agregarArista(Arista _arista) {
    aristas.add(_arista);
  }

  public Set<Arista> aristasDondeEstaVertice(Coordinate _vertice) {
    Set<Arista> ret = new HashSet<>();

    for (Arista arista : aristas) {
      Coordinate v1 = arista.getV1();
      Coordinate v2 = arista.getV2();

      if (v1.equals(_vertice) || v2.equals(_vertice))
        ret.add(arista);
    }

    return ret;
  }

  public double pesoTotal() {
    if (aristas.isEmpty())
      return 0;

    double ret = 0;
    for (Arista arista : aristas)
      ret += arista.getPeso();

    return ret;
  }

  public Set<Arista> getAristas() {
    return aristas;
  }

}
