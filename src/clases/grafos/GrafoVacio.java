package clases.grafos;

import java.util.HashSet;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class GrafoVacio {

  private Set<Coordinate> vertices;

  public GrafoVacio() {
  }

  public GrafoVacio(Set<Coordinate> _vertices) {
    this.vertices = new HashSet<>(_vertices);
  }

  public Set<Coordinate> getVertices() {
    return new HashSet<>(vertices);
  }

}
