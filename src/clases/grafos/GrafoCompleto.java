package clases.grafos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class GrafoCompleto {

  private HashMap<Coordinate, HashMap<Coordinate, Double>> grafo;
  private List<Arista> aristas;

  public GrafoCompleto() {
    grafo = new HashMap<>();
    aristas = new ArrayList<>();
  }

  public void crearGrafoCompletoDe(Set<Coordinate> _vertices) {
    if (!_vertices.isEmpty()) {
      establecerVecinos(_vertices);
      establecerPesos();

      generarAristas();
    }
  }

  public HashMap<Coordinate, HashMap<Coordinate, Double>> obtenerGrafo() {
    return new HashMap<>(grafo);
  }

  public List<Arista> getAristas() {
    return new ArrayList<>(aristas);
  }

  public int cantVertices() {
    return obtenerVertices().size();
  }

  public HashMap<Coordinate, Set<Coordinate>> obtenerGrafoSinDistancias() {
    HashMap<Coordinate, Set<Coordinate>> ret = new HashMap<>(grafo.size());

    for (Coordinate v : obtenerVertices())
      ret.put(v, grafo.get(v).keySet());

    return ret;
  }

  public Set<Coordinate> obtenerVertices() {
    return grafo.keySet();
  }

  private void establecerVecinos(Set<Coordinate> _vertices) {
    for (Coordinate vertice : _vertices) {
      HashMap<Coordinate, Double> vecinos;
      vecinos = new HashMap(establecerVecinosDe(vertice, _vertices));

      grafo.put(vertice, vecinos);
    }
  }

  private Map<Coordinate, Double> establecerVecinosDe(
          Coordinate _verticeActual, Set<Coordinate> _vertices) {
    int cantVecinos = _vertices.size() - 2;

    Map<Coordinate, Double> vecinos = new HashMap(cantVecinos);

    for (Coordinate v : _vertices)
      vecinos.put(v, null);

    vecinos.remove(_verticeActual);

    return vecinos;
  }

  private void establecerPesos() {
    for (Coordinate vertice : obtenerVertices()) {

      Set<Coordinate> vecinos = obtenerVecinosDe(vertice).keySet();
      for (Coordinate vecino : vecinos) {
        double latVertice = vertice.getLat();
        double lonVertice = vertice.getLon();
        double latVecino = vecino.getLat();
        double lonVecino = vecino.getLon();

        double distancia = Math.hypot((latVecino - latVertice), (lonVecino - lonVertice));

        establecerDistanciaEntre(vertice, vecino, distancia);
      }
    }
  }

  private void establecerDistanciaEntre(
          Coordinate _vertice, Coordinate _vecino, double _distancia) {
    obtenerVecinosDe(_vertice).replace(_vecino, _distancia);
  }

  private HashMap<Coordinate, Double> obtenerVecinosDe(Coordinate _vertice) {
    return grafo.get(_vertice);
  }

  private void generarAristas() {
    for (Coordinate vertice : grafo.keySet()) {
      for (Coordinate vecino : grafo.get(vertice).keySet()) {
        double distancia = obtenerDistanciaEntre(vertice, vecino);

        Arista arista = new Arista(vertice, vecino, distancia);
        if (!estaEnAristas(arista)) {
          aristas.add(arista);
        }
      }

    }
  }

  private double obtenerDistanciaEntre(Coordinate _vertice, Coordinate _vecino) {
    return grafo.get(_vertice).get(_vecino);
  }

  private boolean estaEnAristas(Arista _arista) {
    for (Arista arista : aristas) {
      if (arista.equals(_arista))
        return true;
    }

    return false;
  }

}
