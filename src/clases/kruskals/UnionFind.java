package clases.kruskals;

import java.util.HashMap;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class UnionFind {

  private HashMap<Coordinate, Coordinate> disjointSets;

  public UnionFind(Set<Coordinate> _vertices) {
    disjointSets = new HashMap<>(_vertices.size());
    crearDisjointSets(_vertices);
  }

  public Coordinate find(Coordinate _v) {
    Coordinate padre = obtenerPadreDe(_v);
    
    return (_v.equals(padre)) ? padre : find(padre);
  }

  public void union(Coordinate _v1, Coordinate _v2) {
    if (!sameComponent(_v1, _v2)) {
      Coordinate componenteV1 = find(_v1);
      Coordinate componenteV2 = find(_v2);

      disjointSets.replace(componenteV2, obtenerPadreDe(componenteV2), componenteV1);
    }
  }

  public boolean sameComponent(Coordinate _v1, Coordinate _v2) {
    return find(_v1).equals(find(_v2));
  }

  public HashMap<Coordinate, Coordinate> getDisjointSets() {
    return disjointSets;
  }

  public Set<Coordinate> obtenerVertices() {
    return disjointSets.keySet();
  }

  private void crearDisjointSets(Set<Coordinate> _vertices) {
    for (Coordinate vertice : _vertices)
      disjointSets.put(vertice, vertice);
  }

  private Coordinate obtenerPadreDe(Coordinate _v) {
    return disjointSets.get(_v);
  }

}
