package clases.kruskals;

import clases.grafos.Arista;
import clases.grafos.AGM;
import clases.grafos.GrafoCompleto;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Kruskals {

  public Kruskals() {
  }

  public AGM kruskalsAlgorithm(GrafoCompleto _grafo) {
    AGM agm = new AGM();

    UnionFind unionFind = new UnionFind(_grafo.obtenerVertices());

    List<Arista> aristasOrdenadas = obtenerAristasOrdenadas(_grafo.getAristas());

    for (Arista arista : aristasOrdenadas) {
      Coordinate v1 = arista.getV1();
      Coordinate v2 = arista.getV2();

      if (!unionFind.sameComponent(v1, v2)) {
        agm.agregarArista(arista);
        unionFind.union(v1, v2);
      }
    }

    return agm;
  }

  private List<Arista> obtenerAristasOrdenadas(List<Arista> _aristas) {
    List<Arista> aristasOrdenadas = new ArrayList<>(_aristas);
    Collections.sort(aristasOrdenadas);
    return aristasOrdenadas;
  }

}
