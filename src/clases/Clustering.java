package clases;

import clases.grafos.AGM;
import clases.grafos.Arista;
import clases.grafos.GrafoCompleto;
import clases.grafos.GrafoVacio;
import clases.kruskals.Kruskals;
import java.util.HashSet;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Clustering {

  private GrafoVacio ultimoGrafoVacioAnalizado;
  private GrafoCompleto ultimoGrafoCompletoAnalizado;
  private AGM ultimoAGMgenerado;

  private double distanciaSignificativa;
  private int aristasEliminadas;

  public Clustering() {
  }

  public Clustering(double _distancia) {
    verificarParametros(_distancia);
    distanciaSignificativa = _distancia;
  }

  public Set<Arista> iniciarClustering(GrafoVacio _grafoVacio) {
    aristasEliminadas = 0;

    if (!_grafoVacio.equals(ultimoGrafoVacioAnalizado)) {
      ultimoGrafoVacioAnalizado = _grafoVacio;
      GrafoCompleto gc = new GrafoCompleto();
      gc.crearGrafoCompletoDe(_grafoVacio.getVertices());

      ultimoGrafoCompletoAnalizado = gc;

      return algoritmoZahn(gc, false);
    }

    return algoritmoZahn(ultimoGrafoCompletoAnalizado, true);
  }

  public int getAristasEliminadas() {
    return aristasEliminadas;
  }

  public void setParametros(double _distancia) {
    verificarParametros(_distancia);
    distanciaSignificativa = _distancia;
  }

  private Set<Arista> algoritmoZahn(GrafoCompleto _grafoCompleto, boolean _grafoCompletoFueAnalizado) {
    if (_grafoCompletoFueAnalizado)
      return eliminarAristas(ultimoAGMgenerado);

    Kruskals kruskals = new Kruskals();
    AGM agm = kruskals.kruskalsAlgorithm(_grafoCompleto);

    ultimoAGMgenerado = agm;

    return eliminarAristas(agm);
  }

  private Set<Arista> eliminarAristas(AGM _agm) {
    Set<Arista> ret = new HashSet<>(_agm.getAristas());
    for (Coordinate vertice : ultimoGrafoVacioAnalizado.getVertices()) {

      Set<Arista> aristasDeVertice = _agm.aristasDondeEstaVertice(vertice);
      for (Arista arista : aristasDeVertice) {
        boolean aristaMayorAvecinos = esSignificativamenteMayor(arista, aristasDeVertice);
        if (aristaMayorAvecinos) {
          ret.remove(arista);
          aristasEliminadas++;
        }
      }
    }

    return ret;
  }

  private boolean esSignificativamenteMayor(Arista _aristaActual, Set<Arista> _aristasVecinas) {
    // Vertice con solo una Arista
    if (_aristasVecinas.size() == 1)
      return false;

    int cantAristasMenores = 0;
    double pesoAristaActual = _aristaActual.getPeso();

    for (Arista aristaVecina : _aristasVecinas) {
      if (!_aristaActual.equals(aristaVecina)) {
        double diferenciaDePeso = pesoAristaActual - aristaVecina.getPeso();

        if (diferenciaDePeso >= distanciaSignificativa)
          cantAristasMenores++;

      }
    }

    int cantAristasVecinas = _aristasVecinas.size();
    double porcentajeMayorAvecinos = cantAristasMenores / (cantAristasVecinas - 1) * 100;

    return porcentajeMayorAvecinos >= 50;
  }

  private void verificarParametros(double _distancia) {
    if (_distancia <= 0)
      throw new RuntimeException("Parámetro/s inválido/s");
  }

}
