package clases;

import clases.grafos.Arista;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.Style;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

public class Mapa {

  private JMapViewer mapViewer;

  public Mapa(int ancho, int alto) {
    mapViewer = new JMapViewer();
    mapViewer.setMinimumSize(new Dimension(ancho, alto));
    mapViewer.setSize(ancho, alto);

    configurarMapa();
  }

  public void insertarVertices(Set<Coordinate> _vertices) {
    removerVertices();
    removerAGM();

    Style style = obtenerStyleDelMarker();

    for (Coordinate c : _vertices) {
      MapMarkerDot marker = new MapMarkerDot(c);
      marker.setStyle(style);

      mapViewer.addMapMarker(marker);
    }
  }

  public void insertarGrafo(Set<Arista> _aristas) {
    removerAGM();

    for (Arista arista : _aristas) {
      Coordinate v1 = arista.getV1();
      Coordinate v2 = arista.getV2();

      List<Coordinate> listArista = new ArrayList<>(3);
      listArista.add(v1);
      listArista.add(v2);
      listArista.add(v1);

      MapPolygon polygon = new MapPolygonImpl(listArista);

      mapViewer.addMapPolygon(polygon);
    }
  }

  public void insertarGrafo(HashMap<Coordinate, Set<Coordinate>> _arbol) {
    removerAGM();

    for (Coordinate vertice : _arbol.keySet()) {
      for (Coordinate vecino : _arbol.get(vertice)) {
        List<Coordinate> arista = new ArrayList<>(3);
        arista.add(vertice);
        arista.add(vecino);
        arista.add(vertice);

        MapPolygon polygon = new MapPolygonImpl(arista);

        mapViewer.addMapPolygon(polygon);
      }
    }
  }

  public void removerVertices() {
    mapViewer.removeAllMapMarkers();
  }

  public void removerAGM() {
    mapViewer.removeAllMapPolygons();
  }

  public JMapViewer getMapa() {
    return mapViewer;
  }

  private void configurarMapa() {
    mapViewer.setAutoscrolls(true);

    mapViewer.setDisplayPosition(new Coordinate(-34.6117, -58.4383), 10);
  }

  private Style obtenerStyleDelMarker() {
    Style style = new Style();
    style.setBackColor(Color.BLUE);
    style.setColor(Color.BLACK);

    return style;
  }

}
